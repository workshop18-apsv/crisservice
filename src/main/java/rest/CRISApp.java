package rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("rest")
public class CRISApp extends ResourceConfig {
	public CRISApp() {
		packages("rest");
	}

}
